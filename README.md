The system is developed in Python/Django 2.0.6 Framework and uses MySQL database as DBMS. It uses MVC architecture.

The packages needed for the development are in requirements_dev.txt.

It’s recommended to use an virtual environment like VirtualEnv for programming.

Main URL Paths:

/ - index/home page

/bios/ - artists list (with pagination)

/biografia/<id> - detail view for the specific artist

/admin - Site Administration page

 - Test Environment

The test environment uses Pytest and Factory Boy for unit and integration testing and Selenium Web Driver for functional testing. In addition, it uses Python-Coverage to make coverage on the files.

 - Database

The MySQL database configuration is setted in settings.py file. To it work, it’s need a MySQL server running on your development environment, a database called “egcm”, with username “egcm” and password “egcm”.

Make shure to run the following commands on MySQL console:

GRANT ALL PRIVILEGES ON *.* TO 'django'@'localhost';

GRANT ALL PRIVILEGES ON egcm.* TO 'egcm'@'localhost';

 - Useful links:

https://docs.djangoproject.com/en/2.1/

https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django