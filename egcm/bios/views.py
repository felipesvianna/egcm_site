from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import UserProfile, WorkLinks, Biography
from .forms import UserProfileForm, WorkLinksForm, BiographyForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required


def index(request):
    """
    View function for home page of site.
    """
    return render(request, 'index.html',)

def bio_detail(request, id=None):
    instance_profile = get_object_or_404(UserProfile, id=id)
    instance_work_links = get_object_or_404(WorkLinks, userprofile_iduserprofile_id=id)
    instance_bio = get_object_or_404(Biography, userprofile_iduserprofile_id=id)

    context = {
        "instance_profile": instance_profile,
        "instance_work_links": instance_work_links,
        "instance_bio": instance_bio,
    }
    return render(request, "bios/bio_detail.html", context)

def bio_list(request):
    queryset_list = UserProfile.objects.all()
    paginator = Paginator(queryset_list, 25) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        queryset = paginator.page(1)
    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)

    context = {
        "obj_list": queryset,
    }
    return render(request, "bios/bio_list.html", context)

@login_required
def bio_create(request):
    current_user = request.user
    profile_form = UserProfileForm(request.POST or None, request.FILES or None)
    work_links_form = WorkLinksForm(request.POST or None)
    bio_form = BiographyForm(request.POST or None)

    if profile_form.is_valid() and bio_form.is_valid() and work_links_form.is_valid():
        instance_profile = profile_form.save(commit=False)
        instance_work_links = work_links_form.save(commit=False)

        instance_profile.user = current_user
        instance_work_links.user = current_user

        instance_profile = profile_form.save()

        instance_bio = bio_form.save(commit=False)
        instance_bio.userprofile_iduserprofile_id = instance_profile.id
        instance_work_links.userprofile_iduserprofile_id = instance_profile.id
        instance_work_links.save()
        bio_form.save()

        messages.success(request, "Successfully created", extra_tags="messages")
        return HttpResponseRedirect(reverse('bio_detail', args=(instance_profile.id,)))

    context = {
        "profile_form": profile_form,
        "work_links_form": work_links_form,
        "bio_form": bio_form,
    }
    return render(request, "bios/bio_create.html", context)

@login_required
def bio_update(request, id=None):
    instance_profile = get_object_or_404(UserProfile, id=id)
    instance_work_links = get_object_or_404(WorkLinks, userprofile_iduserprofile_id=id)
    instance_bio = get_object_or_404(Biography, userprofile_iduserprofile_id=id)

    profile_form = UserProfileForm(request.POST or None, request.FILES or None, instance=instance_profile)
    work_links_form = WorkLinksForm(request.POST or None, instance=instance_work_links)
    bio_form = BiographyForm(request.POST or None, instance=instance_bio)

    if profile_form.is_valid() and bio_form.is_valid() and work_links_form.is_valid():
        instance_profile = profile_form.save(commit=False)
        instance_work_links = work_links_form.save(commit=False)

        instance_profile = profile_form.save()

        instance_bio = bio_form.save(commit=False)
        instance_bio.userprofile_iduserprofile_id = instance_profile.id
        instance_work_links.userprofile_iduserprofile_id = instance_profile.id
        instance_work_links.save()
        bio_form.save()

        messages.success(request, "Successfully updated", extra_tags="messages")
        return HttpResponseRedirect(reverse('bio_detail', args=(instance_profile.id,)))
    else:
        messages.error(request, "Not successfully updated", extra_tags="messages")

    context = {
        "instance_profile": instance_profile,
        "instance_bio": instance_bio,
        "instance_work_links": instance_work_links,
        "profile_form": profile_form,
        "work_links_form": work_links_form,
        "bio_form": bio_form,
    }
    return render(request, "bios/bio_create.html", context)

@login_required
def bio_delete(request, id=None):
    instance = get_object_or_404(UserProfile, id=id)
    instance.delete()
    messages.success(request, "Successfully deleted", extra_tags="messages")
    return HttpResponseRedirect(reverse('bio_list',))
