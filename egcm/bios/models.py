from django.db import models
from django.contrib.auth.models import User

def upload_location(instance, filename):
    return "%s/%s" %(instance.id, filename)

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)
    email = models.CharField(max_length=32)
    country = models.CharField(max_length=32)
    state = models.CharField(max_length=32)
    city = models.CharField(max_length=32)
    avatar_image = models.ImageField(upload_to=upload_location, default="default-avatar.png")
    draft = models.BooleanField(default=False)
    available_profile = models.BooleanField(default=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

    class Meta:
        ordering = ["first_name", "last_name"]

class Biography(models.Model):
    userprofile_iduserprofile = models.OneToOneField(UserProfile, on_delete=models.CASCADE)
    biography_text = models.TextField(max_length=1000)

class WorkLinks(models.Model):
    userprofile_iduserprofile = models.OneToOneField(UserProfile, on_delete=models.CASCADE)
    work_link1 = models.CharField(max_length=2083, null=True, blank=True)
    work_link2 = models.CharField(max_length=2083, null=True, blank=True)
    work_link3 = models.CharField(max_length=2083, null=True, blank=True)
    description_work_link1 = models.CharField(max_length=32, null=True, blank=True)
    description_work_link2 = models.CharField(max_length=32, null=True, blank=True)
    description_work_link3 = models.CharField(max_length=32, null=True, blank=True)
