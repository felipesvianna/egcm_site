from django import forms

from .models import UserProfile, WorkLinks, Biography

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        exclude = ('user', 'available_profile', )

class BiographyForm(forms.ModelForm):
	class Meta:
		model = Biography
		exclude = ('userprofile_iduserprofile',)

class WorkLinksForm(forms.ModelForm):
    class Meta:
        model = WorkLinks
        fields = ['work_link1', 'description_work_link1',
                  'work_link2', 'description_work_link2',
                  'work_link3', 'description_work_link3',
        ]
