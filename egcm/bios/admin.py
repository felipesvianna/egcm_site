from django.contrib import admin

# Register your models here.
from bios.models import UserProfile, WorkLinks, Biography

class UserProfileModelAdmin(admin.ModelAdmin):
    list_display = ["id", "__str__", "country", "state", "city", "draft", "available_profile"]
    list_display_links = ["id", "__str__"]
    list_filter = ["first_name", "country", "state", "city"]
    search_fields = ["first_name", "last_name", "content"]

    class Meta:
        model = UserProfile

class BiographyModelAdmin(admin.ModelAdmin):
    list_display = ["id", "userprofile_iduserprofile", "biography_text"]
    list_display_links = ["id", "userprofile_iduserprofile"]
    search_fields = ["content"]

    class Meta:
        model = Biography

class WorkLinksModelAdmin(admin.ModelAdmin):
    list_display = ["id", "userprofile_iduserprofile", "work_link1", "description_work_link1", "work_link2", "description_work_link2", "work_link3", "description_work_link3"]
    list_display_links = ["id", "userprofile_iduserprofile"]

    class Meta:
        model = WorkLinks

admin.site.register(UserProfile, UserProfileModelAdmin)
admin.site.register(Biography, BiographyModelAdmin)
admin.site.register(WorkLinks, WorkLinksModelAdmin)
