from django.urls import path, include
from django.conf.urls import url
from . import views

from django.contrib.auth.views import (
   password_reset,
   password_reset_done,
   password_reset_confirm,
   password_reset_complete
)

urlpatterns = [
    path('', views.index, name='index'),
]

#CRUD pages
urlpatterns += [
    path('bios/create', views.bio_create, name='bio_create'),
    path('bios/<int:id>', views.bio_detail, name='bio_detail'),
    path('bios/', views.bio_list, name='bio_list'),
    path('bios/<int:id>/edit', views.bio_update, name='bio_update'),
    path('bios/<int:id>/delete', views.bio_delete, name='bio_delete'),
]

#Authentication urls (for login, logout, password management)
urlpatterns += [
    url(r'^accounts/', include('registration.backends.simple.urls')),
    # the new password reset URLs
    url(r'^accounts/password/reset/$',
        password_reset,
        {'template_name':
        'registration/password_reset_form.html'},
        name="password_reset"),
    url(r'^accounts/password/reset/done/$',
        password_reset_done,
        {'template_name':
        'registration/password_reset_done.html'},
        name="password_reset_done"),
    url(r'^accounts/password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        password_reset_confirm,
        {'template_name':
        'registration/password_reset_confirm.html'},
        name="password_reset_confirm"),
    url(r'^accounts/password/done/$',
        password_reset_complete,
        {'template_name':
        'registration/password_reset_complete.html'},
        name="password_reset_complete"),
    #path('accounts/', include('registration.backends.simple.urls')),
]
