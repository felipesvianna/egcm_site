import pytest
from .factories import UserFactory, UserProfileFactory, BiographyFactory, WorkLinksFactory

@pytest.mark.django_db
def test_if_creates_a_simple_user_without_profile():
    user_instance = UserFactory()
    assert user_instance.pk > 0, 'Should create a simple user'

@pytest.mark.django_db
def test_if_creates_a_user_profile():
     user_instance = UserFactory()
     user_profile_instance = UserProfileFactory(user=user_instance)
     assert user_profile_instance.pk > 0, 'Should create a user profile'
