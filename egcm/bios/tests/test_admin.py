import pytest
from .factories import UserFactory, UserProfileFactory, BiographyFactory, WorkLinksFactory
from selenium import webdriver
from django.contrib.admin.sites import AdminSite
from .. import admin
from .. import models

# @pytest.mark.django_db
# def test_if_users_are_listed_correctly_in_admin_page():
#     user_instance = UserFactory()
#     user_profile_instance = UserProfileFactory(user=user_instance)
#     user_profile_name = user_instance.first_name + " " + user_instance.last_name
#
#     browser = webdriver.Firefox()
#     browser.get('http://localhost:8000')
#     browser.find_element_by_link_text('user_profile_name')
