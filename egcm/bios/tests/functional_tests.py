import pytest
from selenium import webdriver

@pytest.mark.django_db
def test_if_opens_the_home_page_in_a_browser():
    browser = webdriver.Firefox()
    browser.get('http://localhost:8000')
    assert 'EGCM - Site' in browser.title, 'Should open the home page'
