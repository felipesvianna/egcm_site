import pytest
from ..views import index

def test_if_render_the_home_page(client):
    response = client.get('/')
    assert response.status_code == 200, 'Should response to be 200 to the home page'

def test_if_render_the_login_page(client):
    response = client.get('/accounts/login/')
    assert response.status_code == 200, 'Should response to be 200 to the login page'

def test_if_render_the_registration_page(client):
    response = client.get('/accounts/register/')
    assert response.status_code == 200, 'Should response to be 200 to the registration page'

@pytest.mark.django_db
def test_if_render_the_biography_listing_page(client):
    response = client.get('/bios/')
    assert response.status_code == 200, 'Should response to be 200 to the biography listing page'
