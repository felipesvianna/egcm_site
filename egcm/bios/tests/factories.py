#Factories for models

import factory
from ..models import UserProfile, Biography, WorkLinks
from django.contrib.auth.models import User

class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Faker('first_name')
    email = factory.Faker('safe_email')
    password = factory.PostGenerationMethodCall('set_password', 'f3l1p31234')
    is_superuser = True
    is_staff = True
    is_active = True

class UserProfileFactory(factory.DjangoModelFactory):
    class Meta:
        model = UserProfile

    user = factory.SubFactory(UserFactory)
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.Faker('email')
    country = factory.Faker('country_code',representation="alpha-2")
    state = factory.Faker('state')
    city = factory.Faker('city')
    avatar_image = factory.django.ImageField(color='blue')

class BiographyFactory(factory.DjangoModelFactory):
    class Meta:
        model = Biography

    userprofile_iduserprofile = factory.SubFactory(UserProfileFactory)
    biography_text = factory.Faker('text', max_nb_chars=200, ext_word_list=None)

class WorkLinksFactory(factory.DjangoModelFactory):
    class Meta:
        model = WorkLinks

    userprofile_iduserprofile = factory.SubFactory(UserProfileFactory)
    work_link1 = factory.Faker('uri')
    work_link2 = factory.Faker('uri')
    work_link3 = factory.Faker('uri')
    description_work_link1 = "Link1"
    description_work_link2 = "Link2"
    description_work_link3 = "Link3"
